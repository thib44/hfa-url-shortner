class AddUnconnectedUserReferenceToReducedUrl < ActiveRecord::Migration[5.2]
  def change
    add_reference :reduced_urls, :unconnected_user, index: true, foreign_key: true
  end
end
