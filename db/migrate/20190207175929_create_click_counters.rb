class CreateClickCounters < ActiveRecord::Migration[5.2]
  def change
    create_table :click_counters do |t|
      t.references :reduced_url, foreign_key: true, index: true

      t.timestamps
    end
  end
end
