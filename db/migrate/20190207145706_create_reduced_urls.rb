class CreateReducedUrls < ActiveRecord::Migration[5.2]
  def change
    create_table :reduced_urls do |t|
      t.string :original_url
      t.string :reduce_hash, unique: true
      t.references :user, foreign_key: true, index: true
      t.datetime :expiration_time
      t.timestamps
    end
  end
end
