class CreateUnconnectedUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :unconnected_users do |t|
      t.string :token

      t.timestamps
    end
  end
end
