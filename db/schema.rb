# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_11_162212) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "click_counters", force: :cascade do |t|
    t.bigint "reduced_url_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reduced_url_id"], name: "index_click_counters_on_reduced_url_id"
  end

  create_table "reduced_urls", force: :cascade do |t|
    t.string "original_url"
    t.string "reduce_hash"
    t.bigint "user_id"
    t.datetime "expiration_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "unconnected_user_id"
    t.index ["unconnected_user_id"], name: "index_reduced_urls_on_unconnected_user_id"
    t.index ["user_id"], name: "index_reduced_urls_on_user_id"
  end

  create_table "unconnected_users", force: :cascade do |t|
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "click_counters", "reduced_urls"
  add_foreign_key "reduced_urls", "unconnected_users"
  add_foreign_key "reduced_urls", "users"
end
