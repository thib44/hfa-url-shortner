require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  describe 'Get #redirect_url' do
    before { @reduced_url = create(:reduced_url) }
    subject { get :redirect_url, params: { hash: @reduced_url.reduce_hash }}

    it 'Should respond with 302' do
      subject
      expect(response).to have_http_status(302)
    end

    it 'Should respond with valid status' do
      @reduced_url.update(expiration_time: Time.now - 2.hours)
      subject
      expect(response).to be_successful
    end

    it 'Should create a ClickCounter' do
      expect{ subject }.to change(ClickCounter, :count).by(1)
    end

    it 'Should respond with valid status' do
      @reduced_url.destroy
      subject
      expect(response).to be_successful
    end

    it 'Should not create a ClickCounter' do
      @reduced_url.destroy
      expect{ subject }.to change(ClickCounter, :count).by(0)
    end
  end
end
