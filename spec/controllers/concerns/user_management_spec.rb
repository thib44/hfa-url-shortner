require 'rails_helper'

class FakesController < ApplicationController
  include UserManagementsConcern
end

describe FakesController, type: :controller do
  describe 'With connected user' do
    before do
      @user = create(:user)
      sign_in @user
    end

    it 'Should return the connected User' do
      expect(subject.get_user_or_unconnected_user).to eq(@user)
      expect(subject.get_user_or_unconnected_user.class).to eq(User)
    end
  end

  describe 'With unconnected user' do
    it 'Create a new Unconnected User' do
      expect { subject.get_user_or_unconnected_user }.to change(UnconnectedUser, :count).by(1)
    end

    it 'Return an unconnected User' do
      expect(subject.get_user_or_unconnected_user.class).to eq(UnconnectedUser)
    end
  end
end
