require 'rails_helper'
RSpec.describe ReducedUrlsController, type: :controller do

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # ReducedUrlsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "POST #create" do
    let(:valid_session) {
      user = create(:user)
      sign_in user
    }
    subject do
      params = { original_url: 'https://www.highflyers.agency/', expiration_delay: 30 }
      post :create, params: params, format: :json, session: valid_session
    end

    it 'Should return a success response' do
      subject
      expect(response).to be_successful
    end

    it 'Return a shorten url' do
      subject
      expect(response.body).to be_a(String)
      expect(response.body).to include('http://localhost:3000')
    end

    it 'Return an error if original_url is empty' do
      params = { original_url: '' }
      post :create, params: params, format: :json
      expect(response).to have_http_status(422)
      expect(JSON.parse(response.body)['errors']).to include('You should should put an url')
    end

    it 'Should create a ReducedUrl' do
      expect { subject }.to change(ReducedUrl, :count).by(1)
    end

    it 'Create a ReduceUrl with expiration_time in 30 minutes' do
      subject
      expect(ReducedUrl.last.expiration_time).to be > Time.now + 29.minutes
    end

    it 'Create a ReduceUrl with expiration time in 3 minutes' do
      params = { original_url: 'https://www.highflyers.agency/' }
      post :create, params: params, format: :json
      expect(ReducedUrl.last.expiration_time).to be < Time.now + 4.minutes
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested reduced_url" do
      user = create(:user)
      sign_in user
      reduced_url = create(:reduced_url, user: user)
      expect {
        delete :destroy, params: {id: reduced_url.to_param}
      }.to change(ReducedUrl, :count).by(-1)
    end
  end

  describe 'Put expire' do
    before do
      user = create(:user)
      sign_in user
      @reduced_url = create(:reduced_url, user: user)
    end

    subject do
      put :expire, params: { reduced_url_id: @reduced_url.id }, session: valid_session
    end

    it 'Should return a successfull response' do
      subject
      expect(response).to be_successful
    end

    it 'Should update the reduced_url#expiration_time' do
      subject
      expect(@reduced_url.reload.expiration_time).to be < Time.now
    end
  end

  describe 'Put extend_duration' do
    before do
      user = create(:user)
      sign_in user
      @reduced_url = create(:reduced_url, user: user)
    end

    subject do
      put :extend_duration,
        params: {
          reduced_url_id: @reduced_url.id,
          expiration_delay: 30
        },
        session: valid_session
    end

    it 'Should return a successfull response' do
      subject
      expect(response).to be_successful
    end

    it 'Should add 30 minutes to expiration_time' do
      subject
      expect(@reduced_url.reload.expiration_time).to be > Time.now + 29.minutes
    end

    it 'Should not work for unconnected user' do
      reduced_url = create(:reduced_url)
      expect { put :extend_duration, params: { reduced_url_id: reduced_url.id }}.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
