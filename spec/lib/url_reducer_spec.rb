require 'rails_helper'
require 'faker'

RSpec.describe UrlReducer, type: :lib do
  describe 'Initializations should have attributes' do

    subject { UrlReducer.new(Faker::Internet.url) }

    it 'Create an instance' do
      expect(subject).to be_a(UrlReducer)
    end

    it 'Should failed without original url' do
      expect { UrlReducer.new }.to raise_error(ArgumentError)
    end

    it 'Have an accessible original_url' do
      expect(subject.original_url).not_to be_empty
    end
  end

  describe 'Test of reduce method' do
    subject do
      @reducer = UrlReducer.new(Faker::Internet.url)
      @reducer.reduce
    end

    it 'Return an reduce url' do
      expect(subject).not_to be_nil
      expect(subject).to be_a(String)
      expect(subject).to include('http://localhost:3000')
    end

    it 'reduce_url should be readable' do
      subject
      expect(@reducer.reduce_url).not_to be_nil
    end

    it 'reduce_hash should be readable' do
      subject
      expect(@reducer.reduce_hash).not_to be_nil
    end
  end
end
