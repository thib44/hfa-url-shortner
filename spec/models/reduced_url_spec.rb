require 'rails_helper'

RSpec.describe ReducedUrl, type: :model do
  it { should validate_presence_of(:original_url) }
  it { should validate_presence_of(:reduce_hash) }
  it { should validate_presence_of(:expiration_time) }
  it { should validate_uniqueness_of(:reduce_hash) }
  it { should belong_to(:user).optional }
  it { should have_many(:click_counters) }
  it { should belong_to(:unconnected_user).optional }

  describe 'Test of creation without user' do
    subject { build(:reduced_url, user: nil) }

    it 'Should be valid' do
      expect(subject).to be_valid
    end
  end

  describe 'Test of is_expired? method' do
    subject { create(:reduced_url) }

    it 'Should return false when is in the period' do
      expect(subject.is_expired?).to be(false)
    end

    it 'Should return true when is out of the period' do
      reduced_url = subject
      reduced_url.update(expiration_time: Time.now - 1.hours)
      expect(subject.is_expired?).to be(true)
    end
  end

  describe 'Test of build_labels method' do
    before { @reduced_url = create(:reduced_url, expiration_time: Time.now + 30.minutes) }

    it 'Should return an array of 31 labels' do
      expect(@reduced_url.build_labels.count).to eq(31)
    end
  end

  describe 'Test validation of expiration_time' do
    it 'Is valid' do
      reduced_url = build(:reduced_url, expiration_time: Time.now + 29.minutes)
      expect(reduced_url).to be_valid
    end

    it 'Is invalid with bigger interval than 30 minutes' do
      reduced_url = build(:reduced_url, expiration_time: Time.now + 40.minutes)
      reduced_url.valid?
      expect(reduced_url.errors[:expiration_time]).to include('Intervall should be smaller than 30 minutes')
    end

    it 'Is valid without user' do
      reduced_url = build(:reduced_url, user:  nil, expiration_time: Time.now + 2.minutes)
      expect(reduced_url).to be_valid
    end

    it 'Is invalid with bigger interval than 30 minutes' do
      reduced_url = build(:reduced_url, user: nil, expiration_time: Time.now + 10.minutes)
      reduced_url.valid?
      expect(reduced_url.errors[:expiration_time]).to include('Intervall should be smaller than 3 minutes')
    end
  end

  describe 'Destroy' do
    before do
      @reduced_url = create(:reduced_url)
      create(:click_counter, reduced_url: @reduced_url)
    end

    subject { @reduced_url.destroy }

    it 'Can be destroy with click_counters' do
      expect(subject).to be_valid
    end
  end
end
