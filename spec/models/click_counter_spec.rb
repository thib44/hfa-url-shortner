require 'rails_helper'

RSpec.describe ClickCounter, type: :model do
  it { should belong_to(:reduced_url) }

  describe 'Test of click_per_minutes method' do
    subject do
      @reduced_url = create(:reduced_url)
      3.times do
        create(:click_counter, reduced_url: @reduced_url)
      end
      ClickCounter.click_per_minutes(@reduced_url)
    end

    it 'Should return an array of 4 values' do
      expect(subject.size).to eq(4)
    end

    it 'Try to work with other minutes' do
      subject
      click_counter = create(:click_counter, reduced_url: @reduced_url)
      click_counter.update(created_at: Time.now + 2.minutes)
      result = ClickCounter.click_per_minutes(@reduced_url)
      expect(result).to eq([3, 0, 1, 0])
    end
  end
end
