require 'rails_helper'

RSpec.describe UnconnectedUser, type: :model do
  it { should validate_presence_of(:token) }
  it { should have_many(:reduced_urls) }
end
