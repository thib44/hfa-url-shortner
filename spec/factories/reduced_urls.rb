require 'faker'
FactoryBot.define do
  factory :reduced_url do
    user
    original_url { Faker::Internet.url }
    reduce_hash { SecureRandom.hex(3) }
    expiration_time { Time.now + 3.minutes }
  end
end
