Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'welcome#index'
  get '/:hash', to: 'welcome#redirect_url', as: :redirect_url
  resources :reduced_urls, only: [:create, :destroy] do
    put 'expire'
    put 'extend_duration'
    get 'stats', to: 'reduced_urls#stats', as: :stats
  end
end
