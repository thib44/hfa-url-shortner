include Rails.application.routes.url_helpers
class UrlReducer
  attr_reader :original_url, :reduce_url, :reduce_hash

  def initialize(original_url)
    @original_url = original_url
    @reduce_url = nil
    @reduce_hash = nil
  end

  # Create a uniq Hash
  # Return the reduced url
  # Set reduce_url and reduce_hash of instance variable
  def reduce
    exist = true
    # Check if the reduce_hash exist if it exist create a new one
    while exist do
      @reduce_hash = SecureRandom.hex(3)
      exist = ReducedUrl.find_by(reduce_hash: @reduce_hash).present?
    end

    @reduce_url = redirect_url_url(@reduce_hash)
  end
end
