import axios from 'axios'
axios.defaults.headers.common['X-CSRF-Token'] = document
      .querySelector('meta[name="csrf-token"]')
      .getAttribute('content');
axios.defaults.headers.common['Accept'] = 'application/json'
