import axios from 'axios';
import { axios_config } from '../plugins/axios_config';
let moment = require('moment');

document.addEventListener('turbolinks:load', () =>{
  $(document.body).on('click', '#list .update-time-link', function(event) {
    var url = $(event.target).data('url');
    var target = $(event.target).parents('tr')
    expireUrl(url).then(data => {
      var value = moment(data.expiration_time).format('DD MMM HH:mm');
      target.find('.expiration-time').html(value);
    })
  });

  function expireUrl(url) {
    return axios.put(url)
    .then(response => {
      return response.data
    })
  }
})
