import axios from 'axios';
import { axios_config } from '../plugins/axios_config';
document.addEventListener('turbolinks:load', () => {
  $(document.body).on('click', '#list .delete-link', function(event) {
    var deleteUrl = event.target.dataset.deleteUrl;
    let target = event.target;
    axios.delete(deleteUrl)
    target.parentElement.parentElement.parentElement.remove()
  })
})
