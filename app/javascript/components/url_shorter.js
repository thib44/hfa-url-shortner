import axios from 'axios';
import { axios_config } from '../plugins/axios_config';
let moment = require('moment');

document.addEventListener('turbolinks:load', () => {
  var submit_button = document.getElementById('submit_original_url');

  $(submit_button).click(function() {
    generateShortenUrl();
  });

  $('#shorter-form .result #copy-url').click(function() {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($('.result #shorten-url').html()).select();
    document.execCommand("copy");
    $temp.remove();
  });

  function generateShortenUrl() {
    var original_url = $('#shorter-form #original_url').val();
    var expiration_delay = $('#shorter-form #expiration_delay').val();
    $('#shorter-form .error').text('');
    $('.result #shorten-url').text('');
    $('#shorter-form .result').addClass('hidden');
    axios.post('/reduced_urls', { original_url: original_url, expiration_delay: expiration_delay })
    .then(response => {
      $('.result #shorten-url').text(response.data.generated_url);
      $('#shorter-form .result').removeClass('hidden');
      var reduced_url = response.data.reduced_url;
      var expiration_time = moment(reduced_url.expiration_time).format('DD MMM HH:mm');
      var created_at = moment(reduced_url.created_at).format('DD MMM HH:mm');
      $('#list table .header').after(`
          <tr>
            <td>${reduced_url.original_url}</td>
            <td>${response.data.generated_url}</td>
            <td>0</td><td>${created_at}</td>
            <td class='expiration-time'>${expiration_time}</td>
            <td>
              <a data-stat-url=${response.data.stat_path} class="stat-link" href="javascript:;">Stats</a>
              <span class=${(response.data.expire_path == undefined) && 'hidden'}>
                <a data-url=${response.data.expire_path} class="update-time-link" href="javascript:;">Expire</a>
                <a class="extend-duration-link" href="javascript:;">Extend</a>
                <a data-delete-url=${response.data.delete_path} class="delete-link" href="javascript:;">Delete</a>
              </span>
            </td>
            <td class='expiration-selector hidden'>
              Extend
              <input
                id="expiration_delay_${reduced_url.id}"
                class='reduce-urls-expiration'
                type='range'
                min=1
                max=30
                value=30
                list='tickmarks'
                oninput="day_delay_${reduced_url.id}.value=expiration_delay_${reduced_url.id}.value"
                data-url=${response.data.extend_path}>
              </input>
              <output id="day_delay_${reduced_url.id}" for='expiration_date'>30</output>
            </td>
          </tr>
        `)
    })
    .catch(error => {
      console.log(error.response);
      $('#shorter-form .error').text(error.response.data.errors);
    })
  }
})
