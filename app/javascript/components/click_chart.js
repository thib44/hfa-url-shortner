import Chart from 'chart.js';
import axios from 'axios';
import { axios_config } from '../plugins/axios_config';

document.addEventListener('turbolinks:load', () => {
  $(document.body).on('click', '#list .stat-link', function(event) {
    event.stopPropagation();
    var statUrl = event.target.dataset.statUrl;
    axios.get(statUrl)
    .then(response => {
      loadChart(response.data);
    })
  });

  function loadChart(data) {
    var ctx = $("#click-stat-chart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data.labels,
            datasets: [{
                label: 'Number of Clicks',
                data: data.values,
                backgroundColor: "#f94e4eb0",
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
  }
})
