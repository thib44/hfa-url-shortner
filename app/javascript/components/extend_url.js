import axios from 'axios';
import { axios_config } from '../plugins/axios_config';
let moment = require('moment');

document.addEventListener('turbolinks:load', () =>{
  $(document.body).on('click', '#list .extend-duration-link', function(event) {
    $(event.target).parents('tr').find('.expiration-selector').removeClass('hidden')
  })

  $(document.body).on('change', '#list .expiration-selector .reduce-urls-expiration', function(event) {
    var target = $(event.target);
    var url = target.data('url');
    var value = target.val()
    axios.put(url, { expiration_delay: value })
    .then(response => {
      var new_expiration = moment(response.data.expiration_time).format('DD MMM HH:mm');
      target.parents('tr').find('.expiration-time').html(new_expiration);
    })
  })
})
