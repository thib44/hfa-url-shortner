class ReducedUrl < ApplicationRecord
  include Rails.application.routes.url_helpers
  validates :original_url, :reduce_hash, :expiration_time, presence: true
  validates :reduce_hash, uniqueness: true
  validate :expiration_time_thirty_minutes, on: :create

  belongs_to :user, optional: true
  belongs_to :unconnected_user, optional: true
  has_many :click_counters, dependent: :destroy

  def is_expired?
    Time.now > expiration_time
  end

  def build_labels
    labels = []
    time = created_at.beginning_of_minute
    while time < expiration_time.end_of_minute do
      labels << time
      time += 1.minutes
    end
    labels
  end

  private

  # Validate than interval between creation time and expiration time is of 30 minutes
  # when they are user relation and of 3 minutes without user
  def expiration_time_thirty_minutes
    if expiration_time.present?
      minutes = (expiration_time - Time.now) / 1.minutes
      if user.present?
        errors.add(:expiration_time, "Intervall should be smaller than 30 minutes") if minutes > 30
      else
        errors.add(:expiration_time, "Intervall should be smaller than 3 minutes") if minutes > 3
      end
    end
  end
end
