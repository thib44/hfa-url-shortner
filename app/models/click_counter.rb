class ClickCounter < ApplicationRecord
  belongs_to :reduced_url

  def self.click_per_minutes(reduced_url)
    clicks = where(reduced_url: reduced_url)
    labels = reduced_url.build_labels
    values = []
    labels.each do |label|
      values << clicks.select { |c| c[:created_at].between?(label, label.end_of_minute) }.size || 0
    end
    values
  end
end
