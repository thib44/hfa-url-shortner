class UnconnectedUser < ApplicationRecord
  validates :token, presence: true
  has_many :reduced_urls
end
