class WelcomeController < ApplicationController
  include UserManagementsConcern

  # get /
  # List your reduced_url (connected and unconnected user)
  def index
    @reduced_urls = get_user_or_unconnected_user.reduced_urls.includes(:click_counters).reverse
  end

  # Get /:hash
  # Redirect you to the original url. If it's not expired
  def redirect_url
    reduced_url = ReducedUrl.find_by(reduce_hash: params['hash'])
    if reduced_url.present? && !reduced_url.is_expired?
      reduced_url.click_counters.create
      redirect_to reduced_url.original_url
    end
  end
end
