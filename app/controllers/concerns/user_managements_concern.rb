module UserManagementsConcern
  include ActiveSupport::Concern

  # If user is signed_in return the current_user
  # if user is not signed_in but cookies present return the UnconnectedUser
  # Else create an UnconnectedUser and return the created UnconnectedUser
  def get_user_or_unconnected_user
    if user_signed_in?
      current_user
    else
      if cookies[:unconnected_users].present?
        @unconnected_users = UnconnectedUser.find_by(token: cookies[:unconnected_users])
      else
        token = SecureRandom.hex
        cookies[:unconnected_users] = token
        UnconnectedUser.create(token: token)
      end
    end
  end
end
