class ReducedUrlsController < ApplicationController
  include UserManagementsConcern
  before_action :set_reduced_url, only: :destroy
  before_action :authenticate_user!, only: [:expire, :extend_duration, :destroy]

  def stats
    set_reduced_url
    @labels = @reduced_url.build_labels.map { |label| label.strftime('%Hh%M') }
    @values = ClickCounter.click_per_minutes(@reduced_url)
  end

  # Put /reduced_urls/:reduced_url_id/expire.json
  # Permit to expire the url. it will put the date to current date
  # Is only accessible to connected user
  def expire
    set_reduced_url
    if @reduced_url.update(expiration_time: Time.now)
      render json: @reduced_url
    else
      render json: @reduced_url.errors, status: :unprocessable_entity
    end
  end

  # Put /reduced_urls/:reduced_url_id/extend_duration.json
  # Permit to extend the duration of the url by adding your time to CurrentTime
  # Is only accessible to connected user
  def extend_duration
    set_reduced_url
    if @reduced_url.update(expiration_time: Time.now + params[:expiration_delay].to_i.minutes)
      render json: @reduced_url
    else
      render json: @reduced_url.errors, status: :unprocessable_entity
    end
  end

  # POST /reduced_urls
  # POST /reduced_urls.json
  # Create reduced_url.
  def create
    url = params[:original_url]
    if url.present?
      @url_reducer = UrlReducer.new(url)
      @url_reducer.reduce
      build_reduced_url
      if @reduced_url.save
        params = create_response_params
        render json: params
      else
        render json: @reduced_url.errors, status: :unprocessable_entity
      end
    else
      render json: { errors: 'You should should put an url'}, status: 422
    end
  end

  # DELETE /reduced_urls/1
  # DELETE /reduced_urls/1.json
  def destroy
    @reduced_url.destroy
    respond_to do |format|
      format.html { redirect_to reduced_urls_url, notice: 'Reduced url was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Params wich is respond by the create method
  # If user is signed_in it have more params
  def create_response_params
    params = {
        generated_url: @url_reducer.reduce_url,
        reduced_url: @reduced_url,
        stat_path: reduced_url_stats_path(@reduced_url),
      }
    if user_signed_in?
      params[:expire_path] = reduced_url_expire_path(@reduced_url)
      params[:extend_path] = reduced_url_extend_duration_path(@reduced_url)
      params[:delete_path] = reduced_url_path(@reduced_url)
    end
    params
  end

  # Build the new ReducedUrl Instance
  def build_reduced_url
    @reduced_url =  get_user_or_unconnected_user
                    .reduced_urls
                    .build(reduce_url_attributes)
  end

  # Build the expiration_time, if user is not connected it's 3min, if he is
  # Connected it's function of his choice
  def build_expiration_time
    if user_signed_in?
      Time.now + params[:expiration_delay].to_i.minutes
    else
      Time.now + 3.minutes
    end
  end

  # Attributes to build reduced_url
  def reduce_url_attributes
    {
      original_url: @url_reducer.original_url,
      reduce_hash: @url_reducer.reduce_hash,
      expiration_time: build_expiration_time
    }
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_reduced_url
      @reduced_url = get_user_or_unconnected_user.reduced_urls.find(params[:id] || params[:reduced_url_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reduced_url_params
      params.fetch(:reduced_url, {})
    end
end
