json.extract! reduced_url, :id, :created_at, :updated_at
json.url reduced_url_url(reduced_url, format: :json)
